#include "tracker.h"
#include "iostream"

using namespace std;

Tracker::Tracker()
{
    Person p1;
    _person = p1;
}

Tracker::Tracker(Person p1, string name){
    _person = p1;
    _fout.open(name.c_str());
}

void Tracker::predict(){
    _person.predict();
}

Person Tracker::get_person(){
    return _person;
}


void Tracker::print(Person& measured, vector<int>& locations,vector<int>& movements){
    vector<Moments> moms; //moments for right and left hands
    double hu_rh[7]; // 7-hu moments for right hand
    double hu_lh[7]; // 7-hu moments for left hand

    moms.push_back(measured.get_right_hand().get_moments());
    moms.push_back(measured.get_left_hand().get_moments());
    HuMoments(moms[0],hu_rh); // right hand
    HuMoments(moms[1],hu_lh); // left hand

    // print moments for right hand
//    _fout << _person.get_face().get_blob_center() << '\t';
//    _fout << _person.get_right_hand().get_blob_center() << '\t';
//    _fout << _person.get_right_hand().get_angle() << '\t';

    _fout << moms[0].m00 << ',';
    _fout << moms[0].m01 << ',';
    _fout << moms[0].m02 << ',';
    _fout << moms[0].m03 << ',';
    _fout << moms[0].m10 << ',';
    _fout << moms[0].m11 << ',';
    _fout << moms[0].m12 << ',';
    _fout << moms[0].m20 << ',';
    _fout << moms[0].m21 << ',';
    _fout << moms[0].m30 << ',';

    //_fout << moms[0].mu00 << ',';
   // _fout << moms[0].mu01 << ',';
    _fout << moms[0].mu02 << ',';
    _fout << moms[0].mu03 << ',';
    //_fout << moms[0].mu10 << ',';
    _fout << moms[0].mu11 << ',';
    _fout << moms[0].mu12 << ',';
    _fout << moms[0].mu20 << ',';
    _fout << moms[0].mu21 << ',';
    _fout << moms[0].mu30 << ',';

//    _fout << moms[0].nu00 << ',';
//    _fout << moms[0].nu01 << ',';
    _fout << moms[0].nu02 << ',';
    _fout << moms[0].m03 << ',';
//    _fout << moms[0].nu10 << ',';
    _fout << moms[0].nu11 << ',';
    _fout << moms[0].nu12 << ',';
    _fout << moms[0].nu20 << ',';
    _fout << moms[0].nu21 << ',';
    _fout << moms[0].nu30 << ',';

    for(int i = 0; i < 7; i++){
        _fout << hu_rh[i] << ',';
    }

    //repeat for left hand
//    _fout << _person.get_left_hand().get_blob_center() << '\t';
//    _fout << _person.get_left_hand().get_angle() << '\t';

    //locations
    for(int i = 0; i < locations.size(); i++){
        _fout << locations[i] << ',';
    }
    //movements
    for(int i = 0; i < movements.size(); i++){
        _fout << movements[i] << ',';
    }
    _fout << "gsl\n";

}

void Tracker::correct(Person& measured){
    Mat_<float> face_measurement(2,1);
    Mat_<float> rhand_measurement(2,1);
    Mat_<float> lhand_measurement(2,1);

    face_measurement(0) = static_cast<float>(measured.get_face().get_blob_center().x);
    face_measurement(1) = static_cast<float>(measured.get_face().get_blob_center().y);

    rhand_measurement(0) = static_cast<float>(measured.get_right_hand().get_blob_center().x);
    rhand_measurement(1) = static_cast<float>(measured.get_right_hand().get_blob_center().y);

    lhand_measurement(0) = static_cast<float>(measured.get_left_hand().get_blob_center().x);
    lhand_measurement(1) = static_cast<float>(measured.get_left_hand().get_blob_center().y);

    _person.correct(face_measurement,
                   rhand_measurement,
                   lhand_measurement);

    double rh_orient = measured.get_right_hand().get_angle();
    double lh_orient = measured.get_left_hand().get_angle();

    _person.set_orientations(0,rh_orient,lh_orient);

}

void Tracker::correct2(Person& measured){
    Mat_<float> face_measurement(2,1);
    Mat_<float> rhand_measurement(2,1);
    Mat_<float> lhand_measurement(2,1);

    face_measurement(0) = static_cast<float>(measured.get_face().get_blob_center().x);
    face_measurement(1) = static_cast<float>(measured.get_face().get_blob_center().y);

    rhand_measurement(0) = static_cast<float>(measured.get_right_hand().get_blob_center().x);
    rhand_measurement(1) = static_cast<float>(measured.get_right_hand().get_blob_center().y);

    lhand_measurement(0) = static_cast<float>(measured.get_left_hand().get_blob_center().x);
    lhand_measurement(1) = static_cast<float>(measured.get_left_hand().get_blob_center().y);

    _person.correct(face_measurement,
                   rhand_measurement,
                   lhand_measurement);

    Blob f = measured.get_face();
    Blob rh = measured.get_right_hand();
    Blob lh = measured.get_left_hand();
    _person.correct2(f,rh,lh);
}
