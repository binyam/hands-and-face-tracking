#ifndef TRACKER_H
#define TRACKER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <vector>
#include <fstream>
#include "person.h"

using namespace cv;
using namespace std;

class Tracker
{
public:
    Tracker();
    Tracker(Person p1, string fname);
    void predict();
    void correct(Person& measured);
    void correct2(Person& measured);
    Person get_person();
    void print(Person& p,vector<int>& locations,vector<int>& movements);

private:
    Person _person;
    ofstream _fout;
};

#endif // TRACKER_H
